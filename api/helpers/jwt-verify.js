const jwt = require('jsonwebtoken');

module.exports = {


  friendlyName: 'JWT Verify',


  description: '',


  inputs: {
    token: {
      type: 'string'
    },
    callback: {
      type: 'ref'
    }
  },


  exits: {

  },


  fn: async (inputs, exits) => {
    try{
      exits.success(jwt.verify(inputs.token, sails.config.custom.secret))
    }catch (err) {
      exits.error(err.message)
    }
  }
};

