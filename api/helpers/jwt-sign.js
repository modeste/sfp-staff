let jwt = require('jsonwebtoken');

module.exports = {


  friendlyName: 'JWT Sign',


  description: '',


  inputs: {
    payload: {
      type: 'json',
      required: true,
    },
    expiresIn: {
      type: 'string',
      required: true,
    }
  },

  exits: {
    
  },


  fn: async (inputs, exits) => {
    return exits.success(jwt.sign({ data: inputs.payload }, sails.config.custom.secret, {expiresIn: inputs.expiresIn}));
  }
  //example expiresIn: https://github.com/zeit/ms
};

