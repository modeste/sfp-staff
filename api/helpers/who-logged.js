
module.exports = {


    friendlyName: 'Fetch Who Request',

    description: '',

    inputs: {
        headers: {
            type: 'ref',
            required: true,
        },
    },

    exits: {

    },

    fn: async function (inputs, exits) {
        let user = {}
        if (inputs.headers.authorization) {
            let parts = inputs.headers.authorization.split(' ');
            let credentials = parts[1];
            let jwtToken = await sails.helpers.jwtVerify.with({ token: credentials })

            user = await User.findOne({ username: jwtToken.data.username })
                .populate('person')
            return exits.success(user)
        } else {
            return exits.success(user)
        }
    }
};







