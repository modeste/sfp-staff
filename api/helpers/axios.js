let axios = require('axios');

module.exports = {


  friendlyName: 'Axios',


  description: 'HTTP requests',


  inputs: {
    method: {
      type: 'string',
      example: 'post'
    },
    baseUrlDownloads: {
      type: 'string',
      required: true,
    },
    queryUrl: {
      type: 'string',
    },
    headers: {
      type: {},
    },
    data: {
      type: {},
    },
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    let response = axios({
      method: inputs.method,
      url: inputs.baseUrlDownloads + inputs.queryUrl,
      headers: inputs.headers,
      data: inputs.data,
    });

    return exits.success({data: response.data, status: response.status});
  }
};

