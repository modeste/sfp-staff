const nodemailer = require('nodemailer')
const Promise = require('promise')
const ejs = require("ejs");
const fs = require("fs");

module.exports = {
  friendlyName: 'Send mail',
  description: '',
  inputs: {
    mail: {
      type: 'ref'
    }
  },
  exits: {
  },


  fn: async (inputs, exits) => {
    let noReply = "SFP Staff - ne pas répondre <" + sails.config.custom.authMailUser + ">"

    let transporter;
    if (inputs.mail.who === "no-reply") {
      transporter = nodemailer.createTransport({
        host: 'premium50.web-hosting.com',
        port: 465,
        secure: true,
        auth: {
          user: sails.config.custom.authMailUser,
          pass: sails.config.custom.authMailPass
        }
      })
    }

    let formatmail = {}, emailAccountTemplatesPath = require('path').resolve(sails.config.appPath, 'api/emailTemplates/' + inputs.mail.type + '/html.ejs'), compiledHTML

    compiledHTML = ejs.render(fs.readFileSync(emailAccountTemplatesPath, "utf8"), {
      data: inputs.mail.data,
    })

    formatmail = {
      from: inputs.mail.from ? inputs.mail.from : noReply,
      to: inputs.mail.to,
      cc: inputs.mail.cc ? inputs.mail.cc : "",
      bcc: inputs.mail.bcc ? inputs.mail.bcc : "",
      subject: inputs.mail.subject,
      text: inputs.mail.text ? inputs.mail.text : "",
      html: compiledHTML,
      replyTo: inputs.mail.replyTo ? inputs.mail.replyTo : sails.config.custom.authMailUser,
      attachments: inputs.mail.attachments ? inputs.mail.attachments : [],
    }

    transporter.sendMail(formatmail, (error, info) => {
      if (error) {
        console.log("error send mail...", error)
        exits.error(error)
      } else {
        console.log("info", info)
        exits.success(info)
      }
    })
  }
};

