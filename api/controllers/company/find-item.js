module.exports = {

    friendlyName: 'Item',

    description: '',

    inputs: {
        companyId: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let company = await Company.findOne({ id: inputs.companyId })
            .populate('workers')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!company) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "companyNotFound", message: "Filiale inexistante", field: [{}] }] } };
        }

        if (company.whoCreated) {
            company.whoCreated.person = await Person.findOne({ id: company.whoCreated.person })
        }
        if (company.whoUpdated) {
            company.whoUpdated.person = await Person.findOne({ id: company.whoUpdated.person })
        }

        return exits.success({ status: "success", data: company, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
