module.exports = {

    friendlyName: 'Update',

    inputs: {
        companyId: {
            type: 'string',
            required: true
        },
        name: {
            type: 'string',
            required: true
        },
        phone: {
            type: 'string',
            required: true,
        },
        addres: {
            type: 'string',
            required: true,
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })

        let company = await Company.findOne({ id: inputs.companyId })
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });
        if (!company) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "companyNotFound", message: "Filiale inexistante", field: [{}] }] } };
        }

        let companyRequest = {
            name: inputs.name,
            phone: inputs.phone,
            addres: inputs.addres,
            whoUpdated: userLogged.id,
        }
        company = await Company.update({ id: inputs.companyId }).set(companyRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()

        company = await Company.findOne({ id: inputs.companyId })
            .populate('workers')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });
        
        if (company.whoCreated) {
            company.whoCreated.person = await Person.findOne({ id: company.whoCreated.person })
        }
        if (company.whoUpdated) {
            company.whoUpdated.person = await Person.findOne({ id: company.whoUpdated.person })
        }

        return exits.success({ status: "success", data: company, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
