module.exports = {

    friendlyName: 'Create',

    inputs: {
        name: {
            type: 'string',
            required: true
        },
        phone: {
            type: 'string',
            required: true,
        },
        addres: {
            type: 'string',
            required: true,
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })

        let companyRequest = {
            name: inputs.name,
            phone: inputs.phone,
            addres: inputs.addres,
            whoCreated: userLogged.id,
            whoUpdated: userLogged.id,
        }

        let company = await Company.create(companyRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()

        company = await Company.findOne({ id: company.id })
            .populate('workers')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (company.whoCreated) {
            company.whoCreated.person = await Person.findOne({ id: company.whoCreated.person })
        }
        if (company.whoUpdated) {
            company.whoUpdated.person = await Person.findOne({ id: company.whoUpdated.person })
        }

        return exits.success({ status: "success", data: company, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
