
module.exports = {

    friendlyName: 'Remove',

    inputs: {
        companyId: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        if (inputs.companyId) {
            let company = await Company.findOne({ id: inputs.companyId })
                .intercept((err) => {
                    throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
                });

            if (!company) {
                throw { badRequest: { status: "error", data: {}, errors: [{ code: "companyNotFound", message: "Filiale inexistante", field: [{}] }] } };
            }
            await Company.archive({ id: inputs.companyId })
                .intercept((err) => {
                    throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
                })

            return exits.success({ status: "success", data: [], errors: [{ code: "", message: "", field: [{}] }] })
        }
    }
}
