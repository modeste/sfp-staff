let moment = require('moment');
module.exports = {
  friendlyName: 'List all',

  description: '',

  inputs: {
    startDate: {
      type: 'string',
    },
    endDate: {
      type: 'string',
    },
    limit: {
      type: 'string',
      required: true
    },
    skip: {
      type: 'number',
      required: true
    }
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let criteria = {}
    let company = []

    if(inputs.startDate && !inputs.endDate) {
      let x = []

      let y = await Company.find()
      if(y.length){
        y.forEach(a => {
          let createdAtCurrent = moment(a.createdAt).format('YYYY-MM-DD')

          if(createdAtCurrent >= inputs.startDate) {
            x.push(a.id)
          }
        })
      }
      criteria = {...criteria, id: x }
    }

    if(inputs.startDate && inputs.endDate) {  
      let x = []

      let y = await Company.find()
      if(y.length){
        y.forEach(a => {
          let createdAtCurrent = moment(a.createdAt).format('YYYY-MM-DD')

          if(createdAtCurrent >= inputs.startDate && createdAtCurrent <= inputs.endDate) {
            x.push(a.id)
          }
        })
      }
      criteria = {...criteria, id: x }
    }

    if(!inputs.startDate && inputs.endDate) {  
      let x = []

      let y = await Company.find()
      if(y.length){
        y.forEach(a => {
          let createdAtCurrent = moment(a.createdAt).format('YYYY-MM-DD')

          if(createdAtCurrent <= inputs.endDate) {
            x.push(a.id)
          }
        })
      }
      criteria = {...criteria, id: x }
    }

    if (inputs.limit == "Tout") {
      company = await Company.find(criteria)
        .sort([
          { createdAt: 'DESC' },
        ])
        .populate('workers')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
        });
    } else {
      company = await Company.find(criteria)
        .limit(parseInt(inputs.limit, 10))
        .skip(inputs.skip)
        .sort([
          { createdAt: 'DESC' },
        ])
        .populate('workers')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
        });
    }
    let total = await Company.count(criteria)

    let promises = []

    company.forEach(function (item, index) {
      promises.push((async function (item) {
        if (item.whoCreated) {
          item.whoCreated.person = await Person.findOne({ id: item.whoCreated.person })
        }
        if (item.whoUpdated) {
          item.whoUpdated.person = await Person.findOne({ id: item.whoUpdated.person })
        }
      })(item))
    })
    await Promise.all(promises)

    let data = {
      list: company,
      total
    }

    return exits.success({ status: "success", data, errors: [{ code: "", message: "", field: [{}] }] })
  }
};
