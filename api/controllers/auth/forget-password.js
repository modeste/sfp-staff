let apikey = require('apikeygen').apikey;

module.exports = {

    friendlyName: 'Forget',

    inputs: {
        username: {
            type: 'string',
            required: true
        },
        from: {
            type: 'string',
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let user = await User.findOne({ username: inputs.username, visibility: true })
            .populate('person')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!user) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur n'a aucun compte, compte inexistant", field: [{}] }] } }
        }
        let adminUsersType = ['super_administrator_sys', 'administrator_sys', 'rh', 'dsi', 'it', 'da', 'dg']
        let isValid = false

        if (adminUsersType.indexOf(user.type) >= 0 && inputs.from === "admin") {
            isValid = true
        } else {
            isValid = false
        }

        if (isValid) {
            // apply reset pwd
            let password = apikey(8)
            pwdHash = await sails.helpers.passwords.hashPassword(password)

            await User.update({ username: inputs.username }).set({ password: pwdHash })
                .intercept((err) => {
                    throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
                }).fetch()

            let subject = `Réinitialisation de votre mot de passe [SFP STAFF]`

            let link = sails.config.custom.baseUrlAdmin

            await sails.helpers.sendSysMail.with({
                mail: {
                    type: "infosIdUser",
                    who: "no-reply",
                    to: user.email,
                    subject,
                    data: {
                        ...user, pwd: password,
                        ...user.person,
                        team: "SFP STAFF",
                        link,
                        baseUrl: sails.config.custom.baseUrl,
                        baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
                        finalWordBefore1: `Une fois connecté(e), vous pourrez à tout moment personnaliser votre mot de passe en vous rendant dans la rubrique "Mon profil".`,
                        finalWordBefore2: "Nous vous prions d'agréer nos salutations distinguées.",
                        finalWord: "Cordialement,",
                        msg: `Nous avons le plaisir de vous informer que le mot de passe de votre compte chez SFP STAFF vient d'être réinitialisé et vous pourrez accéder à votre compte avec les éléments ci-dessous :`
                    },
                }
            }).intercept((err) => {
                return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
            })

            return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
        } else {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Cet user ne peut pas se connecter sur cette app", field: [{}] }] } }
        }
    }
}
