let moment = require('moment');

module.exports = {

  friendlyName: 'Login user',

  description: '',

  inputs: {
    username: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let user = await User.findOne({ username: inputs.username, visibility: true })
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      });

    if (!user) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Username n'a aucun compte, compte inexistant", field: [{}] }] } }
    }

    try {
      await sails.helpers.passwords.checkPassword(inputs.password, user.password)
    } catch (err) {
      console.log("err ", err)
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userPasswordNotCorrect", message: "Le mot de passe n'est pas correct", field: [{}] }] } }
    }

    if (user.status == 'inactive') {
      let message = `Votre compte est désactivé, veuillez contacter l'administrateur à l'adresse contact@sfp-staff.com`
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userAccountDeactivate", message, field: [{}] }] } }
    }
    if (user.status == 'waiting') {
      let message = `Votre compte est en attente de validation, veuillez contacter l'administrateur à l'adresse contact@sfp-staff.com`
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userAccountWaiting", message, field: [{}] }] } }
    }

    const jwtToken = await sails.helpers.jwtSign.with({
      payload: { username: user.username },
      expiresIn: '28d'
    })

    await TokenManager.create({ token: jwtToken, expiredAt: moment(new Date()).add(28, 'd').format('x'), user: user.id })

    await User.update({ id: user.id }).set({ lastLoginAt: moment(new Date()).format('x') })
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      })

    user = await User.findOne({ username: user.username })
      .populate('person')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      })

    user = _.merge(user, {
      token: jwtToken
    })

    return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
  }
};
