let moment = require('moment');
module.exports = {

  friendlyName: 'Change password',

  description: '',

  inputs: {
    username: {
      type: 'string',
      required: true
    },
    oldPassword: {
      type: 'string',
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    notAuthorized: {
      description: 'When user have a bad credentials',
      responseType: 'forbidden'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let user = await User.findOne({ username: inputs.username })
      .populate('person')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      });
    if (!user) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur n'a aucun compte, compte inexistant", field: [{}] }] } }
    }

    await sails.helpers.passwords.checkPassword(inputs.oldPassword, user.password)

    hash = await sails.helpers.passwords.hashPassword(inputs.password)
    let userRequest = {
      password: hash,
    }
    user = await User.update({ username: inputs.username }).set(userRequest)
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      }).fetch()

    user = await User.findOne({ username: inputs.username })
      .populate('person')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      });

    let link = sails.config.custom.baseUrlAdmin

    await sails.helpers.sendSysMail.with({
      mail: {
        type: "simple",
        who: "no-reply",
        to: user.email,
        subject: `Mot de passe mis à jour [SFP STAFF]`,
        data: {
          team: "SFP STAFF",
          ...user,
          ...user.person,
          isChangePwd: true,
          link,
          baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
          baseUrl: sails.config.custom.baseUrl,
          finalWordBefore1: `Une fois connecté(e), vous pourrez à tout moment personnaliser votre mot de passe en vous rendant dans la rubrique "Mon profil".`,
          finalWordBefore2: "Nous vous prions d'agréer nos salutations distinguées.",
          finalWord: "Cordialement,",
          msg: "Cette notification a été envoyée à l'adresse e-mail associée à votre compte à des fins de sécurité.",
          content: `Le mot de passe de votre compte a été modifié.`,
        },
      }
    }).intercept((err) => {
      return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
    })

    return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
  }
}
