module.exports = {

    friendlyName: 'Create',

    inputs: {
        civility: {
            type: 'string',
            required: true,
        },
        firstname: {
            type: 'string',
            required: true,
        },
        lastname: {
            type: 'string',
            required: true,
        },
        birthday: {
            type: 'string',
            required: true,
        },
        placeOfBithday: {
            type: 'string',
            required: true,
        },
        adress: {
            type: 'string',
            required: true,  
        },
        email: {
            type: 'string',
            required: true,
        },
        phone: {
            type: 'string',
            required: true,
        },
        nationality: {
            type: 'string',
            required: true,
        },
        cnss: {
            type: 'string',
            required: true,
        },
        languages: {
            type: 'string',
            required: true,
        },
        identity: {
            type: 'ref',
            required: true,
        },
        company: {
            type: 'ref',
        },
        children: {
            type: 'ref',
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })

        let workerRequest = {
            civility: inputs.civility,
            firstname: inputs.firstname,
            lastname: inputs.lastname,
            birthday: inputs.birthday,
            placeOfBithday: inputs.placeOfBithday,
            adress: inputs.adress,
            email: inputs.email,
            phone: inputs.phone,
            nationality: inputs.nationality,
            cnss: inputs.cnss,
            languages: inputs.languages,
            identity: inputs.identity,
            company: inputs.company,
            whoCreated: userLogged.id,
            whoUpdated: userLogged.id,
        }

        let worker = await Worker.create(workerRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()
        
        let promises = []

        inputs.children.forEach(function (child, index) {
            promises.push((async function (child) {
                if (child.isValid) {
                    await await Child.create({ ...child, civility: child.civility.id, worker: worker.id })
                }
            })(child))
        })
        await Promise.all(promises)

        worker = await Worker.findOne({ id: worker.id })
            .populate('company')
            .populate('children')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (worker.whoCreated) {
            worker.whoCreated.person = await Person.findOne({ id: worker.whoCreated.person })
        }
        if (worker.whoUpdated) {
            worker.whoUpdated.person = await Person.findOne({ id: worker.whoUpdated.person })
        }

        return exits.success({ status: "success", data: worker, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
