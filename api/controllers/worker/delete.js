
module.exports = {

    friendlyName: 'Remove',

    inputs: {
        workerId: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        if (inputs.workerId) {
            let worker = await Worker.findOne({ id: inputs.workerId })
                .intercept((err) => {
                    throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
                });

            if (!worker) {
                throw { badRequest: { status: "error", data: {}, errors: [{ code: "workerNotFound", message: "Travailleur inexistant", field: [{}] }] } };
            }
            await Worker.archive({ id: inputs.workerId })
                .intercept((err) => {
                    throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
                })

            return exits.success({ status: "success", data: [], errors: [{ code: "", message: "", field: [{}] }] })
        }
    }
}
