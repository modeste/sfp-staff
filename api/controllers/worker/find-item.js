module.exports = {

    friendlyName: 'Item',

    description: '',

    inputs: {
        workerId: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let worker = await Worker.findOne({ id: inputs.workerId })
            .populate('company')
            .populate('children')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!worker) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "workerNotFound", message: "Travailler inexistant", field: [{}] }] } };
        }
        if (worker.whoCreated) {
            worker.whoCreated.person = await Person.findOne({ id: worker.whoCreated.person })
        }
        if (worker.whoUpdated) {
            worker.whoUpdated.person = await Person.findOne({ id: worker.whoUpdated.person })
        }

        return exits.success({ status: "success", data: worker, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
