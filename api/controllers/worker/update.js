module.exports = {

    friendlyName: 'Update',

    inputs: {
        workerId: {
            type: 'string',
            required: true
        },
        civility: {
            type: 'string',
            required: true,
        },
        firstname: {
            type: 'string',
            required: true,
        },
        lastname: {
            type: 'string',
            required: true,
        },
        birthday: {
            type: 'string',
            required: true,
        },
        placeOfBithday: {
            type: 'string',
            required: true,
        },
        adress: {
            type: 'string',
            required: true,
        },
        email: {
            type: 'string',
            required: true,
        },
        phone: {
            type: 'string',
            required: true,
        },
        nationality: {
            type: 'string',
            required: true,
        },
        cnss: {
            type: 'string',
            required: true,
        },
        languages: {
            type: 'string',
            required: true,
        },
        identity: {
            type: 'ref',
            required: true,
        },
        company: {
            type: 'ref',
        },
        children: {
            type: 'ref',
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })

        let worker = await Worker.findOne({ id: inputs.workerId })
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });
        if (!worker) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "workerNotFound", message: "Travailleur inexistant", field: [{}] }] } };
        }

        let workerRequest = {
            civility: inputs.civility,
            firstname: inputs.firstname,
            lastname: inputs.lastname,
            birthday: inputs.birthday,
            placeOfBithday: inputs.placeOfBithday,
            adress: inputs.adress,
            email: inputs.email,
            phone: inputs.phone,
            nationality: inputs.nationality,
            cnss: inputs.cnss,
            languages: inputs.languages,
            identity: inputs.identity,
            company: inputs.company,
            whoUpdated: userLogged.id,
        }
        worker = await Worker.update({ id: inputs.workerId }).set(workerRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()

        let ids = []
        let promises = []
        inputs.children.forEach(function (child, index) {
            promises.push((async function (child) {

                if (child.id) {
                    await Child.update({ id: child.id }).set({ ...child, civility: child.civility.id, })
                    ids.push(child.id)
                } else {
                    let x = await await Child.create({ ...child, civility: child.civility.id, worker: inputs.workerId }).fetch()
                    ids.push(x.id)
                }
            })(child))
        })
        await Promise.all(promises)

        await Worker.replaceCollection(inputs.workerId, 'children').members(ids)

        worker = await Worker.findOne({ id: inputs.workerId })
            .populate('company')
            .populate('children')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (worker.whoCreated) {
            worker.whoCreated.person = await Person.findOne({ id: worker.whoCreated.person })
        }
        if (worker.whoUpdated) {
            worker.whoUpdated.person = await Person.findOne({ id: worker.whoUpdated.person })
        }

        return exits.success({ status: "success", data: worker, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
