let moment = require('moment');
module.exports = {
  friendlyName: 'List all',

  description: '',

  inputs: {
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let data = []

    data.push(
        {
            label: "Filiales",
            value: await Company.count()
        }
    )
    data.push(
        {
            label: "Employé(e)s",
            value: await Worker.count()
        }
    )
    data.push(
        {
            label: "Utilisateurs",
            value: await User.count({ visibility: true, type: ['administrator_sys', 'rh', 'dsi', 'it', 'da', 'dg'] })
        }
    )

    return exits.success({ status: "success", data, errors: [{ code: "", message: "", field: [{}] }] })
  }
};
