let apikey = require('apikeygen').apikey;

module.exports = {
 
  friendlyName: 'Create',

  inputs: {
    firstname: {
      type: 'string',
      required: true
    },
    lastname: {
      type: 'string',
      required: true
    },
    phone: {
      type: 'string',
    },
    gender: {
      type: 'string',
      isIn: ['M', 'F'],
    },
    birthday: {
      type: 'string',
    },
    addres: {
      type: 'string',
    },
    userAcces: {
      type: 'ref',
    },
    username: {
      type: 'string',
      unique: true,
      required: true
    },
    email: {
      type: 'string',
      isEmail: true,
    },
    type: {
      type: 'string',
    },
    status: {
      type: 'string',
      isIn: ['active', 'inactive', 'waiting'],
      defaultsTo: 'active',
    },
    intermediaries: {
      type: 'ref',
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let userLogged = await sails.helpers.whoLogged.with({
      headers: this.req.headers
    })
    if (!inputs.username) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNameEmpty", message: "Username manquant", field: "username" }] } }
    }

    let user = await User.findOne({ username: inputs.username })
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      })

    if (user) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNameExistError", message: "Nom d'utilisateur existe déjà", field: [{}] }] } }
    }

    let personRequest = {
      firstname: inputs.firstname,
      lastname: inputs.lastname,
      phone: inputs.phone,
      gender: inputs.gender,
      birthday: inputs.birthday,
      userAcces: inputs.userAcces,
      addres: inputs.addres,
    }

    let person = await Person.create(personRequest)
      .fetch()
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      })

    let password = apikey(8)
    pwdHash = await sails.helpers.passwords.hashPassword(password)

    let userRequest = {
      email: inputs.email,
      username: inputs.username,
      password: pwdHash,
      type: inputs.type,
      status: inputs.status,
      person: person.id,
      whoCreated: userLogged.id,
      whoUpdated: userLogged.id,
    }

    user = await User.create(userRequest)
      .fetch()
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      })

    let subject = `Création de votre espace [SFP STAFF]`
    let msg = `Nous avons le plaisir de vous informer que votre compte chez SFP STAFF vient d'être créé et vous pourrez y accéder avec les éléments ci-dessous :`

    let link = sails.config.custom.baseUrlAdmin

    await sails.helpers.sendSysMail.with({
      mail: {
        type: "infosIdUser",
        who: "no-reply",
        to: user.email,
        subject,
        data: {
          ...user, pwd: password,
          ...personRequest,
          team: "SFP STAFF",
          link, 
          baseUrl: sails.config.custom.baseUrl,
          baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
          finalWordBefore1: `Une fois connecté(e), vous pourrez à tout moment personnaliser votre mot de passe en vous rendant dans la rubrique "Mon profil".`,
          finalWordBefore2: "Nous vous prions d'agréer nos salutations distinguées.",
          finalWord: "Cordialement,",
          msg
        },
      }
    }).intercept((err) => {
      return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
    })

    user = await User.findOne({ id: user.id })
      .populate('person')
      .populate('whoCreated')
      .populate('whoUpdated')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      });

    if (user.whoCreated) {
      user.whoCreated.person = await Person.findOne({ id: user.whoCreated.person })
    }
    if (user.whoUpdated) {
      user.whoUpdated.person = await Person.findOne({ id: user.whoUpdated.person })
    }

    return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] });
  }
}
