
module.exports = {

    friendlyName: 'Update',

    inputs: {
        userId: {
            type: 'string',
            required: true
        },
        email: {
            type: 'string',
        },
        firstname: {
            type: 'string',
            required: true
        },
        lastname: {
            type: 'string',
            required: true
        },
        phone: {
            type: 'string',
        },
        gender: {
            type: 'string',
            isIn: ['M', 'F'],
        },
        civility: {
            type: 'string',
        },
        birthday: {
            type: 'string',
        },
        addres: {
            type: 'string',
        },
        userAcces: {
            type: 'ref',
        },
        type: {
            type: 'string',
        },
        avatar: {
            type: 'string',
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })
        let user = await User.findOne({ id: inputs.userId, visibility: true })
            .populate('person')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!user) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur inexistant", field: [{}] }] } };
        }

        let userRequest = {
            type: inputs.type,
            whoUpdated: userLogged.id,
        }
        if (inputs.email !== user.email) {
            userRequest = {
                ...userRequest,
                email: inputs.email,
            }
        }
        await User.update({ id: inputs.userId }).set(userRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()


        let subject = `Modification de votre compte [SFP STAFF]`

        let link = sails.config.custom.baseUrlAdmin

        let personRequest = {
            firstname: inputs.firstname,
            lastname: inputs.lastname,
            civility: inputs.civility,
            phone: inputs.phone,
            gender: inputs.gender,
            avatar: inputs.avatar,
            userAcces: inputs.userAcces,
            addres: inputs.addres,
            birthday: inputs.birthday,
        }

        await Person.update({ id: user.person.id }).set(personRequest)
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()

        let toEmail = inputs.email !== user.email ? `${inputs.email}, ${user.email}` : inputs.email
        user = await User.findOne({ id: inputs.userId })
            .populate('person')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            })

        // send mail
        await sails.helpers.sendSysMail.with({
            mail: {
                type: "infosIdUser",
                who: "no-reply",
                to: toEmail,
                subject,
                data: {
                    ...user,
                    email: inputs.email,
                    ...user.person,
                    pwd: "",
                    team: "SFP STAFF",
                    link,
                    baseUrl: sails.config.custom.baseUrl,
                    baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
                    finalWord: "Cordialement,",
                    content: "Si vous n'êtes pas à l'origine de cette opération, merci de vérifier que vos informations personnelles sont bien à jour en",
                    msg: `Nous avons le plaisir de vous informer que votre compte chez SFP STAFF vient de subir une modification de vos informations personnelles et vous pourrez y accéder avec les éléments de connexion habituels :`
                },
            }
        }).intercept((err) => {
            return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
        })

        user = await User.findOne({ id: inputs.userId })
            .populate('person')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            })

        if (user.whoCreated) {
            user.whoCreated.person = await Person.findOne({ id: user.whoCreated.person })
        }
        if (user.whoUpdated) {
            user.whoUpdated.person = await Person.findOne({ id: user.whoUpdated.person })
        }
        return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
