let apikey = require('apikeygen').apikey;

module.exports = {

    friendlyName: 'Initialize',

    inputs: {
        username: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })
        let user = await User.findOne({ username: inputs.username, visibility: true })
            .populate('person')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!user) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur n'a aucun compte, compte inexistant", field: [{}] }] } }
        }

        let password = apikey(8)
        pwdHash = await sails.helpers.passwords.hashPassword(password)

        await User.update({ username: inputs.username }).set({ password: pwdHash, whoUpdated: userLogged.id, })
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            }).fetch()
        
        let subject = `Réinitialisation de votre mot de passe [SFP STAFF]`
        
        let link = sails.config.custom.baseUrlAdmin

        await sails.helpers.sendSysMail.with({
            mail: {
                type: "infosIdUser",
                who: "no-reply",
                to: user.email,
                subject,
                data: {
                    ...user,
                    ...user.person,
                    pwd: password,
                    team: "SFP STAFF",
                    link,
                    baseUrl: sails.config.custom.baseUrl,
                    baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
                    finalWord: "Cordialement,",
                    msg: `Nous avons le plaisir de vous informer que le mot de passe de votre compte chez SFP STAFF vient d'être réinitialisé et vous pourrez accéder à votre compte avec les éléments ci-dessous :`
                },
            }
        }).intercept((err) => {
            return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
        })

        user = await User.findOne({ username: inputs.username })
            .populate('person')
            .populate('whoCreated')
            .populate('whoUpdated')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (user.whoCreated) {
            user.whoCreated.person = await Person.findOne({ id: user.whoCreated.person })
        }
        if (user.whoUpdated) {
            user.whoUpdated.person = await Person.findOne({ id: user.whoUpdated.person })
        }

        return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
    }
}
