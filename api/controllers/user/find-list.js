module.exports = {
  friendlyName: 'List all',

  description: '',

  inputs: {
    type: {
      type: 'ref',
      required: true
    },
    limit: {
      type: 'string',
      required: true
    },
    skip: {
      type: 'number',
      required: true
    }
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let criteria = { visibility: true }
    let users = []

    if(inputs.type) {
      criteria = {...criteria, type: inputs.type }
    }

    if (inputs.limit == "Tout") {
      users = await User.find(criteria)
        .sort([
          { createdAt: 'DESC' },
        ])
        .populate('person')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
        });
    } else {
      users = await User.find(criteria)
        .limit(parseInt(inputs.limit, 10))
        .skip(inputs.skip)
        .sort([
          { createdAt: 'DESC' },
        ])
        .populate('person')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
        });
    }
    let total = await User.count(criteria)

    let promises = []

    users.forEach(function (item, index) {
      promises.push((async function (item) {
        if (item.whoCreated) {
          item.whoCreated.person = await Person.findOne({ id: item.whoCreated.person })
        }
        if (item.whoUpdated) {
          item.whoUpdated.person = await Person.findOne({ id: item.whoUpdated.person })
        }
      })(item))
    })
    await Promise.all(promises)
 
    let data = {
      list: users,
      total
    }

    return exits.success({ status: "success", data, errors: [{ code: "", message: "", field: [{}] }] })
  }
};
