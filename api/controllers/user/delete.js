
module.exports = {

    friendlyName: 'Remove',

    inputs: {
        userId: {
            type: 'string',
            required: true
        },
    },

    exits: {
        badRequest: {
            description: 'Something is missing',
            responseType: 'badRequest'
        },
        notFound: {
            statusCode: 404,
            responseType: 'notFound'
        },
        success: {
            description: 'Success',
            responseType: '',
            statusCode: 200
        }
    },

    fn: async function (inputs, exits) {
        let userLogged = await sails.helpers.whoLogged.with({
            headers: this.req.headers
        })
        let user = await User.findOne({ id: inputs.userId, visibility: true })
            .populate('person')
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
            });

        if (!user) {
            throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur n'a aucun compte, compte inexistant", field: [{}] }] } }
        }
        await User.update({ id: inputs.userId }).set({ visibility: false, whoUpdated: userLogged.id, })
            .intercept((err) => {
                throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
            })
 
        return exits.success({ status: "success", data: [], errors: [{ code: "", message: "", field: [{}] }] })
    }
}
