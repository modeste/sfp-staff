module.exports = {

  friendlyName: 'Enable or Disable account',

  description: '',

  inputs: {
    userId: {
      type: 'string',
      required: true
    },
    status: {
      type: 'string',
      isIn: ['active', 'inactive', 'waiting'],
      defaultsTo: 'active',
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let user = await User.findOne({ id: inputs.userId, visibility: true })
      .populate('person')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } };
      });

    if (!user) {
      throw { badRequest: { status: "error", data: {}, errors: [{ code: "userNotFound", message: "ID n'a aucun compte, compte inexistant", field: [{}] }] } };
    }
    await User.update({ id: inputs.userId }).set({ status: inputs.status })
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      }).fetch()


    let subject = ""
    let content = ""
    let msg = ""
    let crrMail = "contact@sfp-staff.com"

    if (inputs.status == "active") {
      subject = `Activation de votre compte [SFP STAFF]`
      content = `Nous vous informons que votre compte chez SFP STAFF vient d'être activé.`
      msg = `Pour plus d'information ${crrMail}`
    } else if (inputs.status == "inactive") {
      subject = `Désactivation de votre compte [SFP STAFF]`
      content = `Nous vous informons que votre compte chez SFP STAFF vient d'être désactivé`
      msg = `Pour plus d'information ${crrMail}`
    } else if (inputs.status == "waiting") {
      subject = `Suspension temporaire de votre compte [SFP STAFF]`
      content = `Nous vous informons que votre compte chez SFP STAFF vient d'être suspendu`
      msg = `Pour plus d'information ${crrMail}`
    }

    let link = sails.config.custom.baseUrlAdmin

    user = await User.findOne({ id: inputs.userId })
      .populate('person')
      .populate('whoCreated')
      .populate('whoUpdated')
      .intercept((err) => {
        throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
      })

    // send mail
    await sails.helpers.sendSysMail.with({
      mail: {
        type: "simple",
        who: "no-reply",
        to: user.email,
        subject,
        data: {
          team: "SFP STAFF",
          ...user,
          ...user.person,
          link,
          baseUrl: sails.config.custom.baseUrl,
          baseUrlDownloadsFiles: sails.config.custom.baseUrlDownloadsFiles,
          finalWord: "Cordialement,",
          content,
          msg,
        },
      }
    }).intercept((err) => {
      return exits.error(400, { status: "error", data: {}, errors: [{ code: "sendMailError", message: "Impossible d'envoyer le mail a l'adresse", field: [{}], err }] })
    })

    if (user.whoCreated) {
      user.whoCreated.person = await Person.findOne({ id: user.whoCreated.person })
    }
    if (user.whoUpdated) {
      user.whoUpdated.person = await Person.findOne({ id: user.whoUpdated.person })
    }

    return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
  }
}
