module.exports = {

  friendlyName: 'Get Data',

  description: '',

  inputs: {
    userId: {
      type: 'string',
    },
  },

  exits: {
    badRequest: {
      description: 'Something is missing',
      responseType: 'badRequest'
    },
    notFound: {
      statusCode: 404,
      responseType: 'notFound'
    },
    success: {
      description: 'Success',
      responseType: '',
      statusCode: 200
    }
  },

  fn: async function (inputs, exits) {
    let user
    if(inputs.userId) {
      user = await User.findOne({ id: inputs.userId, visibility: true })
        .populate('person')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
        })
    }
    if(inputs.userCode) {
      user = await User.findOne({ userCode: inputs.userCode, visibility: true })
        .populate('person')
        .populate('whoCreated')
        .populate('whoUpdated')
        .intercept((err) => {
          throw { badRequest: { status: "errorSys", data: {}, errors: [{ code: "errorSys", message: "Erreur système", field: [{}], err }] } }
        })
    }

    if (!user) {
      return exits.success({ status: "success", data: {}, errors: [{ code: "userNotFound", message: "Utilisateur n'a aucun compte, compte inexistant", field: [{}] }] })
    }

    if (user.whoCreated) {
      user.whoCreated.person = await Person.findOne({ id: user.whoCreated.person })
    }
    if (user.whoUpdated) {
      user.whoUpdated.person = await Person.findOne({ id: user.whoUpdated.person })
    }

    return exits.success({ status: "success", data: user, errors: [{ code: "", message: "", field: [{}] }] })
  }
}
