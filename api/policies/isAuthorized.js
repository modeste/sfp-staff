let moment = require('moment');
module.exports = async function (req, res, next) {

  if (!req.headers.authorization) {
    return res.status(401).json({code:'authorizationIsRequired', message: 'No Authorization header was found' })
  }

  let  parts = req.headers.authorization.split(' ');
  if (parts.length !== 2) {
    return res.status(401).json({code:'invalidFormatTokenError', message: 'Format is Authorization: Bearer [token]'})
  }
  let scheme = parts[0];
  let credentials = parts[1];

  if (!/^Bearer$/i.test(scheme)) {
    return res.status(401).json({code:'invalidFormatTokenError', message: 'Format is Authorization: Bearer [token]'})
  }

  let token  = await TokenManager.findOne({
    token: credentials
  })

  if (token) {
    if (token.expiredAt) {
      if(moment(token.expiredAt).format('x') < moment(new Date()).format('x')) {
        return res.status(401)
        .json({
          code: 'tokenExpiredError',
          message: 'token expired'
        })
      }
    }
    if(token.isRevoked){
      return res.status(401)
      .json({
        code: 'tokenRevokedError',
        message: 'token revoked'
      })
    }
  }
  req.token = token

  try {
    const jwtToken = await sails.helpers.jwtVerify.with({ token: credentials })
    if(!jwtToken){
      return res.status(401)
      .json({
        code: 'invalidTokenError',
        message: 'Invalid Token'
      })
    }

    let user = await User.findOne({ username: jwtToken.data.username })

    if (user.status != "active") {
      return res.status(401)
      .json({
        code: 'userNotActiveError',
        message: 'User account not active'
      })
    }

    req.user = user

    next();
  } catch(e) {
    return res.status(401)
    .json({
      code:'invalidTokenError',
      message: 'token expired'
    });
  }

};
