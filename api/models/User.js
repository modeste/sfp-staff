/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    username: {
      type: 'string',
      unique: true,
      required: true
    },
    email: {
      type: 'string',
      isEmail: true,
    },
    password: {
      type: 'string',
      required: true
    },
    type: {
      type: 'string',
      isIn: [
        'none', 'super_administrator_sys', 'administrator_sys', 'rh', 'dsi', 'it', 'da', 'dg',
      ],
      defaultsTo: 'none',
    },
    status: {
      type: 'string',
      isIn: ['active', 'inactive', 'waiting'],
      defaultsTo: 'waiting',
    },
    lastLoginAt: {
      type: 'number',
    },
    token: {
      type: 'string',
    },
    visibility: {
      type: 'boolean',
      defaultsTo: true,
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    person: {
      model: 'person',
    },
    whoCreated: {
      model: 'user',
    },
    whoUpdated: {
      model: 'user',
    }, 
    
  },

  customToJSON: function() {
    return _.omit(this, ['password'])
  },
}; 

