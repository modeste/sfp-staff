/**
 * Worker.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    civility: {
      type: 'string',
    },
    firstname: {
      type: 'string',
      required: true,
    },
    lastname: {
      type: 'string',
      required: true,
    },
    birthday: {
      type: 'string',
      required: true,
    },
    placeOfBithday: {
      type: 'string',
      required: true,
    },
    adress: {
      type: 'string',
      required: true,
    },
    email: {
      type: 'string',
      required: true,
    },
    phone: {
      type: 'string',
      required: true,
    },
    nationality: {
      type: 'string',
      required: true,
    },
    cnss: {
      type: 'string',
      required: true,
    },
    languages: {
      type: 'string',
      required: true,
    },
    identity: {
      type: 'ref',
      required: true,
    },
    
    

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    company: {
      model: 'company',
    },
    children: {
      collection: 'child',
      via: 'worker'
    },
    whoCreated: {
      model: 'user',
    },
    whoUpdated: {
      model: 'user',
    },
  },

};

