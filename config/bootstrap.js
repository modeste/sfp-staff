let apikey = require('apikeygen').apikey;
let moment = require('moment')

/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function (cb) {

  let username = "modeste"
  let email = "gougbedjimodeste@gmail.com"
  let pwd = apikey(8) 
  let user = await User.findOne({ username })

  if(user) {
    console.log("Super Admin existe déja avec Success")
  } else {

    let personRequest = {
      firstname: "Modeste",
      lastname: "Gougbedji",
    }
    let person = await Person.create(personRequest).fetch()
    hash = await sails.helpers.passwords.hashPassword(pwd)

    let userRequest = {
      email,
      username,
      type: "super_administrator_sys",
      password: hash,
      status: "active",
      person: person.id,
    }
    user = await User.create(userRequest)
      .fetch()

    if (user) {
      await sails.helpers.sendSysMail.with({
        mail: {
          type: "infosIdUser",
          who: "no-reply",
          to: user.email,
          subject: `Création de votre espace`,
          data: {
            ...user, pwd,
            ...personRequest,
            team: "SFP STAFF",
            link: sails.config.custom.baseUrlAdmin,
            baseUrl: sails.config.custom.baseUrl,
            baseUrlDownloads: sails.config.custom.baseUrlDownloads,
            finalWord: "Cordialement,",
            finalWordBefore: "Nous vous prions d'agréer, Madame, Monsieur, l'expression de nos salutations distinguées.",
            msg: "Nous avons le plaisir de vous informer que votre compte chez SFP STAFF vient d'être créé et vous pourrez y accéder avec les éléments ci-dessous :"
          },
        }
      })

      console.log("Super Admin Create with Success")
    } else {
      console.log("Erreur création du Super Admin")
    }
  }
  cb();

};
 