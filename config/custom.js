/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  /***************************************************************************
  *                                                                          *
  * Any other custom config this Sails app should use during development.    *
  *                                                                          *
  ***************************************************************************/
  // mailgunDomain: 'transactional-mail.example.com',
  // mailgunSecret: 'key-testkeyb183848139913858e8abd9a3',
  // stripeSecret: 'sk_test_Zzd814nldl91104qor5911gjald',
  // …

  secret: 'fdfsdfdsf4erdtfgvhjkdsf8956895',
  hashSecretKK: `784d5fds6`,

  baseUrlAdmin: "https://sfp-staff.com/",

  baseUrl: 'https://backend.sfp-staff.com/',
  baseUrlDownloadsFiles: `https://backend.sfp-staff.com/downloads/`,

  authMailUser: "noreply@sfp-staff.com",
  authMailPass: '7?R-N=kmPni7',

  formatDate : function (dateString) {
    if (dateString != undefined) {
      var p = dateString.split(/\D/g)
      return [p[2], p[1], p[0]].join("/")
    }
  },
};
