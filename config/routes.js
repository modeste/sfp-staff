/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
 
  // Auth
  "POST /api/v1/auth/signin": "auth/login",
  "POST /api/v1/auth/forget-pwd": "auth/forget-password",

  // Logged
  "POST /api/v1/auth/change-pwd": "auth-logged/change-password",

  // General Settings
  "POST /api/v1/upload-file": "general/upload-file",

  // User
  "POST /api/v1/users/add": "user/create",
  "GET /api/v1/users/item": "user/find-item",
  "GET /api/v1/users/list": "user/find-list",
  "POST /api/v1/users/put": "user/update",
  "POST /api/v1/users/remove": "user/delete",
  "POST /api/v1/users/allow": "user/allow",
  "POST /api/v1/users/initialize": "user/initialize",

  // Company
  "POST /api/v1/company/add": "company/create",
  "GET /api/v1/company/item": "company/find-item",
  "GET /api/v1/company/list": "company/find-list",
  "POST /api/v1/company/put": "company/update",
  "POST /api/v1/company/remove": "company/delete",

  // Worker
  "POST /api/v1/worker/add": "worker/create",
  "GET /api/v1/worker/item": "worker/find-item",
  "GET /api/v1/worker/list": "worker/find-list",
  "POST /api/v1/worker/put": "worker/update",
  "POST /api/v1/worker/remove": "worker/delete",

  // Stats
  "GET /api/v1/stats/all": "stats/all",
  


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
